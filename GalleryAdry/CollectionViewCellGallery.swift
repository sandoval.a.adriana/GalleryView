//
//  CollectionViewCell.swift
//  GalleryAdry
//
//  Created by Adriana Sandoval on 9/29/17.
//  Copyright © 2017 com.sm. All rights reserved.
//

import UIKit

class CollectionViewCellGallery: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
    }
}
